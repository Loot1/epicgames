import React from 'react'
import { Dimensions, Text, View, ImageBackground, FlatList, Image, TextInput } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import Ionicons from 'react-native-vector-icons/Ionicons'

const window = Dimensions.get("window")

const elements = require("./elements.json")

function HomeScreen() {
    const renderItem = ({ item }) => (<Item title={item.title} img={item.img} />)
    return (
        <View style={{ flex: 1 }}>
            <FlatList data={elements} renderItem={renderItem} keyExtractor={item => item.title} />
        </View>
    )
}

const Item = ({ title, img }) => (
    <View style={{ flex: 1 }}>
        <ImageBackground source={{ uri: img }} style={{ width: window.width, height: window.height/4, flex: 1, justifyContent:"center" }}>
            <Text style={{ fontSize: 48, textAlign:"center", textTransform:"uppercase", color:"white", fontWeight:"bold" }}>{title.replace(/ /g,"\n")}</Text>
        </ImageBackground>
    </View>
)

function AccountScreen() {
    return (
        <View style={{ flex: 1, backgroundColor:"#2A2A2A", alignItems:"center" }}>
            <Image source={require('./assets/logo.png')} style={{flex: 1, width:window.height/2, height:window.height/3}}/>
            <Text style={{ fontSize: 28, color:"white", fontWeight:"bold" }}>SE CONNECTER</Text>
            <TextInput
                style={{
                    height: 60,
                    width: window.width-window.width/3,
                    margin: 12,
                    backgroundColor:"#454545",
                    borderRadius: 18
                }}
                placeholder="Adresse e-mail"
                placeholderTextColor = "#7F7F7F"
                autoCompleteType = "email"
            />
            <TextInput
                style={{
                    height: 60,
                    width: window.width-window.width/3,
                    margin: 12,
                    backgroundColor:"#454545",
                    borderRadius: 18
                }} 
                placeholder="Mot de passe"
                placeholderTextColor = "#7F7F7F"
                autoCompleteType = "password"
            />
        </View>
    )
}

const Tab = createBottomTabNavigator()

function MyTabs() {
    return (
        <Tab.Navigator
            tabBarOptions={{
                activeTintColor: 'white',
                inactiveTintColor: 'white',
                style: {backgroundColor:"#2A2A2A"},
                showLabel: false
            }}
        >
            <Tab.Screen
                name="Home"
                component={HomeScreen}
                options={{
                    tabBarIcon: () => (
                        <Image source={require('./assets/logo.png')} style={{width:1200/26, height:1393/26}}/>
                    )
                }}
            />
            <Tab.Screen
                name="Account"
                component={AccountScreen}
                options={{
                    tabBarIcon: ({ color }) => (
                        <Ionicons name="person-circle-outline" size={50} color={color} />
                    )
                }}
            />
        </Tab.Navigator>
    )
}

export default function App() {
    return (
        <NavigationContainer>
            <MyTabs/>
        </NavigationContainer>
    )
}
